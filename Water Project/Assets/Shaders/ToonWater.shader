﻿Shader "Roystan/Toon/Water"
{
	//Adds Property values to the shader
	Properties
	{
		//Values for the material
		//Depth maximum distance property as a cutoff for the gradient
		_DepthGradientShallow("Depth Gradient Shallow", Color) = (0.325, 0.807, 0.971, 0.725)

		_DepthGradientDeep("Depth Gradient Deep", Color) = (0.086, 0.407, 1, 0.749)

		_DepthMaxDistance("Depth Maximum Distance", Float) = 1
		//Color of Foam at the borders of the water
		_FoamColor("Foam Color", Color) = (1,1,1,1)
		//Uses the texture property assigned
		_SurfaceNoise("Surface Noise", 2D) = "white" {}
		//Declare cutoff threshold to get a more binary look.
		_SurfaceNoiseCutoff("Surface Noise Cutoff", Range(0, 1)) = 0.777
		//Animaiton of the water scrolling
		// Property to control scroll speed, in UVs per second.
		_SurfaceNoiseScroll("Surface Noise Scroll Amount", Vector) = (0.03, 0.03, 0, 0)

		// Two channel distortion texture.
		_SurfaceDistortion("Surface Distortion", 2D) = "white" {}
		// Control to multiply the strength of the distortion.
		_SurfaceDistortionAmount("Surface Distortion Amount", Range(0, 1)) = 0.27
		//Maximum and minimum distance of the Foam
		_FoamMaxDistance("Foam Maximum Distance", Float) = 0.4
		_FoamMinDistance("Foam Minimum Distance", Float) = 0.04
	}
		SubShader
		{
				Tags
			{
				"Queue" = "Transparent"
			}
			Pass
			{
			// Transparent "normal" blending.
			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite Off

			CGPROGRAM
			#define SMOOTHSTEP_AA 0.01

			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			// Blends two colors using the same algorithm that our shader is using
			// to blend with the screen. This is usually called "normal blending",
			// and is similar to how software like Photoshop blends two layers.
			float4 alphaBlend(float4 top, float4 bottom)
			{
				float3 color = (top.rgb * top.a) + (bottom.rgb * (1 - top.a));
				float alpha = top.a + bottom.a * (1 - top.a);

				return float4(color, alpha);
			}

			struct appdata
			{
				float4 vertex : POSITION;
				float4 uv : TEXCOORD0;
				float3 normal : NORMAL;
			};
			//Vertex Shader to Fragment Shader Structure
			//Contains information generated by the vertex shader which is passed to the fragment shader.
			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 screenPosition : TEXCOORD2;
				float2 noiseUV : TEXCOORD0;
				float2 distortUV : TEXCOORD1;
				float3 viewNormal : NORMAL;

			};
			 //Declares Noise filters to use on shaders
			 sampler2D _SurfaceNoise;
			 float4 _SurfaceNoise_ST;
			 //Declares the variables of distortion
			 sampler2D _SurfaceDistortion;
			 float4 _SurfaceDistortion_ST;



			v2f vert(appdata v)
			{
				v2f o;

				o.vertex = UnityObjectToClipPos(v.vertex);
				o.screenPosition = ComputeScreenPos(o.vertex);
				o.noiseUV = TRANSFORM_TEX(v.uv, _SurfaceNoise);
				o.distortUV = TRANSFORM_TEX(v.uv, _SurfaceDistortion);
				o.viewNormal = COMPUTE_VIEW_NORMAL;



				return o;
			}
				//Matching property variables declarations
				float4 _DepthGradientShallow;
				float4 _DepthGradientDeep;
				float4 _FoamColor;


				float _DepthMaxDistance;
				float _FoamMaxDistance;
				float _FoamMinDistance;
				float _SurfaceNoiseCutoff;
				float _SurfaceDistortionAmount;

			float2 _SurfaceNoiseScroll;

			sampler2D _CameraDepthTexture;
			sampler2D _CameraNormalsTexture;

			float4 frag(v2f i) : SV_Target
			{
				//samples the depth texture using tex2Dproj and our screen position.
				//Will return the depth of the surface behind our water, in a range of 0 to 1.
				//Converts the non-linear depth to be linear, in world units from the camera.
				float existingDepth01 = tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.screenPosition)).r;

				float existingDepthLinear = LinearEyeDepth(existingDepth01);
				//take the difference between our two depth values, and output the result.
				float depthDifference = existingDepthLinear - i.screenPosition.w;
				//Calculate the color of our water, color depth compared to our maximum depth
				float waterDepthDifference01 = saturate(depthDifference / _DepthMaxDistance);

				float4 waterColor = lerp(_DepthGradientShallow, _DepthGradientDeep, waterDepthDifference01);

				float2 distortSample = (tex2D(_SurfaceDistortion, i.distortUV).xy * 2 - 1) * _SurfaceDistortionAmount;
				// motion and distortion to the waves,
				float2 noiseUV = float2((i.noiseUV.x + _Time.y * _SurfaceNoiseScroll.x) + distortSample.x, (i.noiseUV.y + _Time.y * _SurfaceNoiseScroll.y) + distortSample.y);
				//Sample the noise texture and combine it with our surface color to render waves
				float surfaceNoiseSample = tex2D(_SurfaceNoise, noiseUV).r;
				//Sample the normals buffer in the same way we sampled the depth buffer.
				float3 existingNormal = tex2Dproj(_CameraNormalsTexture, UNITY_PROJ_COORD(i.screenPosition));
				//Waves intensity to increase near the shoreline or where objects intersect the surface of the water, creates foam effect.
				float3 normalDot = saturate(dot(existingNormal, i.viewNormal));
				float foamDistance = lerp(_FoamMaxDistance, _FoamMinDistance, normalDot);
				float foamDepthDifference01 = saturate(depthDifference / foamDistance);
				//cutoff of the noise animation
				float surfaceNoiseCutoff = foamDepthDifference01 * _SurfaceNoiseCutoff;

				float surfaceNoise = smoothstep(surfaceNoiseCutoff - SMOOTHSTEP_AA, surfaceNoiseCutoff + SMOOTHSTEP_AA, surfaceNoiseSample);
				//samples foam color for noise color
				float4 surfaceNoiseColor = _FoamColor;
				surfaceNoiseColor.a *= surfaceNoise;

				// Use normal alpha blending to combine the foam with the surface.
				return alphaBlend(surfaceNoiseColor, waterColor);

			}
			ENDCG
		}
		}
}