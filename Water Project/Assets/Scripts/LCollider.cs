﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LCollider : MonoBehaviour
{
    public GameObject win;
    public Material LwaterShader;
    Vector4 recollideVector = new Vector4(-0.5f, -0.5f, 0f, 0f);
    Vector4 startVector = new Vector4(0.03f, 0.03f, 0f, 0f);

    void Start()
    {
        win.SetActive(false);
        LwaterShader.SetVector("_SurfaceNoiseScroll", startVector);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            LwaterShader.SetVector("_SurfaceNoiseScroll", recollideVector);
            win.SetActive(true);
        }

    }
}
