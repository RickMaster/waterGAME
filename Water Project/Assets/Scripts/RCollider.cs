﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RCollider : MonoBehaviour
{
    public Material RwaterShader;
    Vector4 collideVector = new Vector4(0.5f, 0.5f, 0f, 0f);
    Vector4 startVector = new Vector4(0.03f, 0.03f, 0f, 0f);

    void Start()
    {
        RwaterShader.SetVector("_SurfaceNoiseScroll", startVector);
    }

    // Update is called once per frame
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            RwaterShader.SetVector("_SurfaceNoiseScroll", collideVector);
        }

    }
}
