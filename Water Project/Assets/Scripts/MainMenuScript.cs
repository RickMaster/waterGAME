﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuScript : MonoBehaviour
{
    public GameObject CanvasObject;

    void Start()
    {
        Time.timeScale = 0f;
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void playGame()
    {
        CanvasObject.SetActive(false);
        Time.timeScale = 1f;
    }

    
}
